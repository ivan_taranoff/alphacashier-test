const random = (prefix = "") =>
  "?" +
  (prefix && prefix + "&") +
  "random=" +
  Math.round(Math.random() * 10000);
export const stubPosts = [
  {
    id: 0,
    tags: [0],
    image: "https://picsum.photos/300/300" + random(),
    coloredBlock: false,
    title: "Robot’s Penguin Disguise Keeps Birds",
    text: "Figuring out how our brains work is key to understanding ",
    width: 1,
    height: 1
  },
  {
    id: 1,
    tags: [1],
    image: "https://picsum.photos/300/300" + random(),
    coloredBlock: false,
    title: "5 Things We Learned From X-Men: Days of Future Past",
    text:
      "Peter Dinklage’s porn ‘tache, Jennifer Lawrence’s brilliance and more. Some (tiny) spoilers ahead ",
    width: 2,
    height: 1
  },
  {
    id: 2,
    tags: [5],
    coloredBlock: true,
    title: "14 Things Men Should Never Wear After 30",
    text:
      "Light a bonfire in the garden, and step bravely into your best-dressed decade",
    url: "//ya.ru",
    width: 1,
    height: 1
  },
  {
    id: 3,
    tags: [3, 4],
    coloredBlock: false,
    title: "Комбикорма для уток и свиней оптом",
    text: "Наши корма дают +20% к приросту массы в год",
    width: 1,
    height: 1
  },
  {
    id: 4,
    tags: [1],
    coloredBlock: false,
    image: "https://picsum.photos/300/300" + random(),
    title: "Stunning Changes Along Colorado River",
    text: "Lake Powell offering kayakers new channels to explore",
    width: 1,
    height: 1
  },
  {
    id: 5,
    tags: [0],
    coloredBlock: true,
    title: "14 Things Men Should Never Wear After 30",
    text: "Figuring out how our brains work is key to understanding ",
    url: "#",
    // url: "//ya.ru",
    width: 1,
    height: 2
  },
  {
    id: 6,
    tags: [2],
    coloredBlock: false,
    image: "https://picsum.photos/300/300" + random(),
    title: "Offbeat Portraits Give Stars a New Turn",
    text: "I whipped the Scotch tape from my pocket and said",
    width: 1,
    height: 1
  },
  {
    id: 7,
    tags: [1],
    coloredBlock: false,
    image: "https://picsum.photos/300/300" + random(),
    title: "Must See Places of the New Year",
    text: "Whether it’s India’s literary hub or mountain majesty",
    width: 1,
    height: 1
  },
  {
    id: 8,
    tags: [5],
    coloredBlock: false,
    image: "https://picsum.photos/300/300" + random(),
    title: "Robot’s Penguin Disguise Keeps Birds ",
    text: "Figuring out how our brains work is key to understanding ",
    width: 1,
    height: 1
  },
  {
    id: 9,
    tags: [0],
    coloredBlock: false,
    image: "https://picsum.photos/300/300" + random("grayscale"),
    title: "Stunning Video Reveals Invisible CO2",
    text: "A NASA visualization shows the swirling gas ",
    width: 1,
    height: 1
  }
];
