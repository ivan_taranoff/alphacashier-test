export const stubTags = [
  {
    id: 0,
    name: "TRAVEL",
    color: "#6b3de4"
  },
  {
    id: 1,
    name: "EXPLORERS",
    color: "#d9a34a"
  },
  {
    id: 2,
    name: "NATURE",
    color: "#41a674"
  },
  {
    id: 3,
    name: "СЕЛЬСКОЕ ХОЯЙСТВО",
    color: "#41a674"
  },
  {
    id: 4,
    name: "КОРМА",
    color: "#d8a252"
  },
  {
    id: 5,
    name: "SCIENCE",
    color: "#007ac2"
  }
];
