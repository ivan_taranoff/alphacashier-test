import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import { stubTags } from "@/store/tags";
import { stubPosts } from "@/store/posts";

const loadingDelay = 1000;

export const MUTATIONS = {
  SET_POSTS: "SET_POSTS",
  SET_TAGS: "SET_TAGS",
  ACTIVATE_TAG: "ACTIVATE_TAG",
  DEACTIVATE_TAG: "DEACTIVATE_TAG",
  SET_ACTIVE_TAGS: "SET_ACTIVE_TAGS",
  SET_LOADING: "SET_LOADING"
};

export const ACTIONS = {
  FETCH_TAGS: "FETCH_TAGS",
  FETCH_POSTS: "FETCH_POSTS",
  FETCH_ALL: "FETCH_ALL",
  TOGGLE_TAG: "TOGGLE_TAG"
};

export const GETTERS = {
  SELECTED_TAGS: "SELECTED_TAGS"
};

const store = new Vuex.Store({
  state: {
    isLoading: false,
    selectedTags: [],
    tags: [],
    posts: []
  },
  getters: {
    // route: state => state.route
    [GETTERS.SELECTED_TAGS]: state =>
      state.selectedTags.length ? state.selectedTags : state.tags.map(e => e.id)
  },
  mutations: {
    [MUTATIONS.ACTIVATE_TAG]: (state, tagID) => {
      !~state.selectedTags.indexOf(tagID) && state.selectedTags.push(tagID);
    },
    [MUTATIONS.DEACTIVATE_TAG]: (state, tagID) => {
      const index = state.selectedTags.indexOf(tagID);
      ~index && state.selectedTags.splice(index, 1);
    },
    [MUTATIONS.SET_TAGS]: (state, tags) => {
      state.tags = tags;
    },
    [MUTATIONS.SET_POSTS]: (state, posts) => {
      state.posts = posts;
    },
    [MUTATIONS.SET_LOADING]: (state, value = true) => {
      state.isLoading = value;
    }
  },
  actions: {
    [ACTIONS.FETCH_TAGS]: ({ commit }) => {
      return new Promise(resolve => {
        setTimeout(() => {
          commit(MUTATIONS.SET_TAGS, stubTags);
          resolve();
        }, loadingDelay);
      });
    },
    [ACTIONS.FETCH_POSTS]: ({ commit, getters }, setLoadingFlag = true) => {
      const prom = new Promise(resolve => {
        setTimeout(() => {
          let posts = stubPosts;
          posts = posts.filter(post =>
            post.tags.some(
              postTagID => ~getters[GETTERS.SELECTED_TAGS].indexOf(postTagID)
            )
          );
          commit(MUTATIONS.SET_POSTS, posts);
          resolve();
        }, loadingDelay);
      });
      if (setLoadingFlag) {
        commit(MUTATIONS.SET_LOADING, true);
        prom.then(e => {
          commit(MUTATIONS.SET_LOADING, false);
          return e;
        });
      }

      return prom;
    },

    [ACTIONS.TOGGLE_TAG]: ({ commit, dispatch, state }, tagID) => {
      const foundTag = state.selectedTags.indexOf(tagID);
      if (~foundTag) {
        commit(MUTATIONS.DEACTIVATE_TAG, tagID);
      } else {
        commit(MUTATIONS.ACTIVATE_TAG, tagID);
      }

      return dispatch(ACTIONS.FETCH_POSTS);
    }
  }
  //
  // created(store) {
  //   store.actions.fetchTags().then(() => {
  //     store.actions.fetchPosts();
  //   });
  // }
});

export default store;
